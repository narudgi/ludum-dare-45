﻿//Copyright (c) Ewan Argouse - http://narudgi.github.io/

using DG.Tweening;
using UnityEngine;

namespace ScriptableTween
{
	using static TweenMenu;
	[CreateAssetMenu(fileName = nameof(Restart), menuName = Modules + nameof(Restart), order = Order)]
	public class Restart : TweenModule
	{
		[SerializeField] bool includeDelay = false;
		[SerializeField] int changeDelayTo = -1;

		protected override void Set(Tweener tweener)
		{
			base.Set(tweener);
			tweener?.Restart(includeDelay, changeDelayTo);
		}
	}
}