﻿//Copyright (c) Ewan Argouse - http://narudgi.github.io/

using DG.Tweening;
using UnityEngine;

namespace ScriptableTween
{
	using static TweenMenu;
	[CreateAssetMenu(fileName = nameof(As), menuName = Modules + nameof(As), order = Order)]
	public class As : TweenModule
	{
		[SerializeField] ScriptableObjectTween scriptableObjectTween = default;

		protected override void Set(Tweener tweener)
		{
			base.Set(tweener);
			tweener.SetAs(scriptableObjectTween.tweener);
		}
	}
}