﻿//Copyright (c) Ewan Argouse - http://narudgi.github.io/

using DG.Tweening;
using UnityEngine;

namespace ScriptableTween
{
	using static TweenMenu;
	[CreateAssetMenu(fileName = nameof(Update), menuName = Modules + nameof(Update), order = Order)]
	public class Update : TweenModule
	{
		[SerializeField] UpdateType updateType = UpdateType.Normal;
		[SerializeField] bool isIndependantUpdate = false;

		protected override void Set(Tweener tweener)
		{
			base.Set(tweener);
			tweener?.SetUpdate(updateType, isIndependantUpdate);
		}
	}
}