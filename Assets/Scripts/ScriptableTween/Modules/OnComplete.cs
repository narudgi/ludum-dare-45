﻿//Copyright (c) Ewan Argouse - http://narudgi.github.io/

using DG.Tweening;
using UnityAtoms;
using UnityEngine;

namespace ScriptableTween
{
	using static TweenMenu;

	[CreateAssetMenu(fileName = nameof(OnComplete), menuName = Modules + nameof(OnComplete), order = Order)]
	public class OnComplete : TweenModule
	{
		[SerializeField] VoidEvent onComplete = default;
		[SerializeField] VoidAction[] onCompleteActions = default;

		protected override void Set(Tweener tweener)
		{
			base.Set(tweener);
			tweener.OnComplete(Trigger);
		}

		void Trigger()
		{
			onComplete?.Raise();
			int length = onCompleteActions.Length;
			for (int i = 0; i < length; i++)
			{
				onCompleteActions[i]?.Do();
			}
		}
	}
}