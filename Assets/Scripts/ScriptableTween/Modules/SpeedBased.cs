﻿//Copyright (c) Ewan Argouse - http://narudgi.github.io/

using DG.Tweening;
using UnityEngine;

namespace ScriptableTween
{
	using static TweenMenu;
	[CreateAssetMenu(fileName = nameof(SpeedBased), menuName = Modules + nameof(SpeedBased), order = Order)]
	public class SpeedBased : TweenModule
	{
		[SerializeField] bool isSpeedBased = true;

		protected override void Set(Tweener tweener)
		{
			base.Set(tweener);
			tweener.SetSpeedBased(isSpeedBased);
		}
	}
}