﻿//Copyright (c) Ewan Argouse - http://narudgi.github.io/

using DG.Tweening;
using UnityEngine;

namespace ScriptableTween
{
	using static TweenMenu;

	[CreateAssetMenu(fileName = nameof(Ease), menuName = Modules + nameof(Ease), order = Order)]
	public class Ease : TweenModule
	{
		public enum Type
		{
			Default = 0,
			AmplitudePeriod = 1,
			Overshoot = 2,
			Custom = 3,
		}

		[SerializeField] DG.Tweening.Ease ease = DG.Tweening.Ease.Unset;
		[SerializeField] Type type = Type.Default;

		[Header(nameof(Type.AmplitudePeriod))]
		[SerializeField] float amplitude = default;
		[SerializeField] float period = default;

		[Header(nameof(Type.Overshoot))]
		[SerializeField] float overshoot = default;

		[Header(nameof(Type.Custom))]
		[SerializeField] AnimationCurve customEase = AnimationCurve.Constant(1, 1, 1);

		protected override void Set(Tweener tweener)
		{
			base.Set(tweener);
			switch (type)
			{
				case Type.Default:
					tweener.SetEase(ease);
					break;
				case Type.AmplitudePeriod:
					tweener.SetEase(ease, amplitude, period);
					break;
				case Type.Overshoot:
					tweener.SetEase(ease, overshoot);
					break;
				case Type.Custom:
					tweener.SetEase(customEase);
					break;
			}
		}
	}
}