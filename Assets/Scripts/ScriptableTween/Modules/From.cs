﻿//Copyright (c) Ewan Argouse - http://narudgi.github.io/

using DG.Tweening;
using UnityEngine;

namespace ScriptableTween
{
	using static TweenMenu;
	[CreateAssetMenu(fileName = nameof(From), menuName = Modules + nameof(From), order = Order)]
	public class From : TweenModule
	{
		[SerializeField] bool isRelative = true;

		protected override void Set(Tweener tweener)
		{
			base.Set(tweener);
			tweener.From(isRelative);
		}
	}
}