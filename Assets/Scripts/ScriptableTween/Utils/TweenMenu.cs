﻿//Copyright (c) Ewan Argouse - http://narudgi.github.io/

namespace ScriptableTween
{
	public class TweenMenu
	{
		public const int Order = 1;
		public const string Main = "Tween/";
		public const string Modules = "Tween/Modules/";
	}
}