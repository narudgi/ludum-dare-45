﻿//Copyright (c) Ewan Argouse - http://ewan.design/

using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

namespace ScriptableTween.Editor
{
	using Editor = UnityEditor.Editor;
	[CustomEditor(typeof(ScriptableObjectTween), true)]
	public class ScriptableObjectTweenEditor : Editor
	{
		List<System.Type> moduleTypes = new List<System.Type>();
		string[] modules;
		int moduleIndex = 0;

		private void OnEnable()
		{
			var scriptableObjectTween = (ScriptableObjectTween)target as ScriptableObjectTween;

			moduleTypes.Clear();

			Type moduleBase = typeof(TweenModule);

			var assembly = AppDomain.CurrentDomain.GetAssemblies().SingleOrDefault(a => a.GetName().Name == nameof(ScriptableTween));

			if (moduleBase != null)
			{
				foreach (var t in assembly.GetTypes())
				{
					if (!t.IsAbstract && t.IsSubclassOf(moduleBase))
					{
						moduleTypes.Add(t);
					}
				}
			}
			modules = moduleTypes.Select(m => m.Name).ToArray();
		}

		public override void OnInspectorGUI()
		{
			var scriptableObjectTween = (ScriptableObjectTween)target as ScriptableObjectTween;

			BigLabel(scriptableObjectTween.name);

			base.OnInspectorGUI();

			if (modules == null || modules.Length == 0) return;

			EditorGUILayout.Space();

			EditorGUILayout.BeginVertical(EditorStyles.helpBox, GUILayout.MaxWidth(200));
			{
				GUILayout.FlexibleSpace();
				GUIStyle popup = new GUIStyle(EditorStyles.popup);
				moduleIndex = EditorGUILayout.Popup(moduleIndex, modules, popup);

				GUI.backgroundColor = Color.cyan;
				AddModule(scriptableObjectTween, moduleTypes[moduleIndex]);
			}
			EditorGUILayout.EndVertical();
		}

		public void AddModule(ScriptableObjectTween scriptableObjectTween, Type moduleType)
		{
			if (GUILayout.Button("Add Module"))
			{
				var asset = CreateInstance(moduleType);
				TweenModule assetModule = (TweenModule)asset;
				if (asset == null) return;
				assetModule.name = moduleType.Name;
				assetModule.tween = scriptableObjectTween;
				scriptableObjectTween.tweenModules.Add(assetModule);
				string targetPath = AssetDatabase.GetAssetPath(scriptableObjectTween);


				AssetDatabase.AddObjectToAsset(assetModule, targetPath);
				AssetDatabase.SaveAssets();

				EditorUtility.FocusProjectWindow();

				Selection.activeObject = assetModule;
			}
		}

		public void BigLabel(string name)
		{
			GUIStyle labelStyle = new GUIStyle(EditorStyles.largeLabel);

			labelStyle.fontSize = 24;

			GUILayout.Label(name, labelStyle);
		}
	}
}