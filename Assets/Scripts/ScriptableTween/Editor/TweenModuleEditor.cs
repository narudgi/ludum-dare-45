﻿//Copyright (c) Ewan Argouse - http://ewan.design/

using UnityEditor;
using UnityEngine;

namespace ScriptableTween.Editor
{
	using Editor = UnityEditor.Editor;
	[CustomEditor(typeof(TweenModule), true)]
	public class TweenModuleEditor : Editor
	{
		public override void OnInspectorGUI()
		{
			var tweenModule = (TweenModule)target as TweenModule;

			BigLabel(target.name);

			base.OnInspectorGUI();

			EditorGUILayout.Space();
			GUI.backgroundColor = Color.red;
			if (GUILayout.Button("Destroy", GUILayout.MaxWidth(200)))
			{
				if (tweenModule.tween != null)
				{
					EditorUtility.FocusProjectWindow();
					Selection.activeObject = tweenModule.tween;
				}

				tweenModule.tween.tweenModules.Remove(tweenModule);
				DestroyImmediate(tweenModule, true);

				AssetDatabase.SaveAssets();
				AssetDatabase.Refresh();
			}
		}

		public void BigLabel(string name)
		{
			GUIStyle labelStyle = new GUIStyle(EditorStyles.largeLabel);

			labelStyle.fontSize = 24;

			GUILayout.Label(name, labelStyle);
		}
	}
}