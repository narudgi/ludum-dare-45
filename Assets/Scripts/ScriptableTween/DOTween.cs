using UnityAtoms;
using UnityEngine;

namespace ScriptableTween
{
    public class DOTween : MonoBehaviour
    {
		[SerializeField] BoolReference isActive = default;
		[SerializeField] ScriptableObjectTween scriptableTween = default;

		public void Play()
		{
			if (!isActive.Value) return;
			scriptableTween?.Do(transform);
		}
    }
}