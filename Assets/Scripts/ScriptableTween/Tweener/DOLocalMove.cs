﻿//Copyright (c) Ewan Argouse - http://narudgi.github.io/

using DG.Tweening;
using UnityEngine;

namespace ScriptableTween
{
	using static TweenMenu;
	[CreateAssetMenu(fileName = nameof(DOLocalMove), menuName = Main + nameof(DOLocalMove), order = Order)]
	public class DOLocalMove : DOMove
	{
		public new void Do(Transform transform) => Do(transform?.DOLocalMove(endValue, duration, snapping));
	}
}