﻿//Copyright (c) Ewan Argouse - http://narudgi.github.io/

using DG.Tweening;
using UnityAtoms;
using UnityEngine;

namespace ScriptableTween
{
	using static TweenMenu;
	[CreateAssetMenu(fileName = nameof(DOTimeScale), menuName = Main + nameof(DOTimeScale), order = Order)]
	public class DOTimeScale : ScriptableObjectTween
	{
		[SerializeField] ScriptableObjectTween tween = default;
		[SerializeField] FloatReference endValue = default;
		[SerializeField] FloatReference duration = default;

		public void Do() => Do(this.tween?.tweener?.DOTimeScale(endValue, duration));

		public void Do(ScriptableObjectTween tween) => Do(tween?.tweener?.DOTimeScale(endValue, duration));
	}
}