﻿//Copyright (c) Ewan Argouse - http://narudgi.github.io/

using DG.Tweening;
using UnityAtoms;
using UnityEngine;

namespace ScriptableTween
{
	using static TweenMenu;

	[CreateAssetMenu(fileName = nameof(DOPunchScale), menuName = Main + nameof(DOPunchScale), order = Order)]
	public class DOPunchScale : ScriptableObjectTween
	{
		[SerializeField] Vector3Reference endValue = default;
		[SerializeField] FloatReference duration = default;
		[SerializeField] IntReference vibrato = default;
		[SerializeField] FloatReference elasticity = default;

		public override void Do(Transform transform) => Do(transform?.DOPunchScale(endValue, duration, vibrato, elasticity));
	}
}