﻿//Copyright (c) Ewan Argouse - http://narudgi.github.io/

using DG.Tweening;
using UnityAtoms;
using UnityEngine;

namespace ScriptableTween
{
	using static TweenMenu;
	[CreateAssetMenu(fileName = nameof(DOMove), menuName = Main + nameof(DOMove), order = Order)]
	public class DOMove : ScriptableObjectTween
	{
		[SerializeField] protected Vector3Reference endValue = default;
		[SerializeField] protected FloatReference duration = default;
		[SerializeField] protected BoolReference snapping = default;

		public override void Do(Transform transform)
			=> Do(transform?.DOMove(endValue, duration, snapping));

		public void Do(Transform transform, Vector3 endValue)
			=> Do(transform?.DOMove(endValue, duration, snapping));
	}
}