using UnityEngine;
using UnityEngine.EventSystems;

namespace Game
{
    public class PlateformCreator : MonoBehaviour, IPointerDownHandler
	{
		[SerializeField] Plateform plateform = default;
        [SerializeField] bool destroyPrevious = true;

		static Plateform instance = default;

		public void OnPointerDown(PointerEventData eventData)
		{
			if (eventData == null)
				throw new System.ArgumentNullException(nameof(eventData));

            if (destroyPrevious)
                instance?.Disable();

			instance = Instantiate(
                plateform,
				GetWorldPositionOnPlane(eventData.pressEventCamera, eventData.position, 0f)
				, Quaternion.identity);
		}

        public Vector3 GetWorldPositionOnPlane(Camera main, Vector3 screenPosition, float z)
        {
            Ray ray = main.ScreenPointToRay(screenPosition);
            Plane xy = new Plane(Vector3.forward, new Vector3(0, 0, z));
            float distance;
            xy.Raycast(ray, out distance);
            return ray.GetPoint(distance);
        }

        public static void RemoveReference()
        {
            instance = null;
        }
	}
}