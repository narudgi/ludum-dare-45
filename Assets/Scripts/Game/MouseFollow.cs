using UnityEngine;
using UnityEngine.InputSystem;

namespace Game
{
    public class MouseFollow : MonoBehaviour
    {
        private void Update()
        {
            transform.localPosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }
    }
}