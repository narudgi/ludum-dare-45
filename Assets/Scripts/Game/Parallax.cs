using UnityEngine;

namespace Game
{
    public class Parallax : MonoBehaviour
    {
        [SerializeField] new Transform camera = default;
        [SerializeField] float speedCoefficient = .5f;
        Vector3 lastpos = Vector3.zero;

        void Start()
        {
            lastpos = camera.position;
        }

        void Update()
        {
            transform.position -= ((lastpos - camera.position) * speedCoefficient);
            lastpos = camera.position;
        }
    }
}