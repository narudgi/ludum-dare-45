﻿//Copyright (c) Ewan Argouse - http://narudgi.github.io/

using UltEvents;
using UnityAtoms;
using UnityEngine;
using UnityEngine.Events;

namespace Game
{
	public class Health : AtomMonoBehaviour
	{
		public bool IsAlive
		{
			get => isAlive;
			private set
			{
				isAlive = value;
				isAliveOutput?.SetValue(isAlive);
			}
		}
		public float MaxHealth => maxHealth;
		public float CurrentHealth
		{
			get => currentHealth;
			private set
			{
				currentHealth = value;
				OnHealthChanged?.Invoke(currentHealth);
				healthOutput?.SetValue(currentHealth);
			}
		}

		[SerializeField] private FloatReference maxHealth = default;
		[SerializeField] private FloatVariable healthOutput = default;
		[SerializeField] private BoolVariable isAliveOutput = default;

		public UltFloatEvent OnHealthChanged = default;
		public UltEvent OnHurt = default;
		public UltEvent OnDeath = default;

		private float currentHealth = default;
		private bool isAlive = true;

		public Health()
		{
		}

		private void Awake()
		{
			ResetLife();
		}

		public void Hurt(float dmg)
		{
			if (!IsAlive) return;

			CurrentHealth -= dmg;

			OnHurt?.Invoke();

			if (CurrentHealth > maxHealth.Value)
				CurrentHealth = maxHealth.Value;

			if (CurrentHealth > 0f) return;

			CurrentHealth = 0f;
			IsAlive = false;

			OnDeath?.Invoke();
		}

		public void Hurt(Collider collider, float dmg)
		{
			Hurt(dmg);
		}

		public void ResetLife()
		{
			IsAlive = true;
			CurrentHealth = maxHealth.Value;
		}

		public float GetHealthRatio()
		{
			if (maxHealth.Value <= 0) return 0;
			return CurrentHealth / maxHealth.Value;
		}

		public float GetHealthDifference()
		{
			return maxHealth - CurrentHealth;
		}
	}
}