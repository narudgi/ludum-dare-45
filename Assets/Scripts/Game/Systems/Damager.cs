﻿//Copyright (c) Ewan Argouse - http://narudgi.github.io/

using UnityAtoms;
using UnityEngine;

namespace Game
{
	public class Damager : AtomMonoBehaviour
	{
        [SerializeField] private FloatReference damageAmount = default;
        [SerializeField] private LayerMask damagedLayer = default;

		private void OnTriggerEnter(Collider other) => Damage(other);
		private void OnTriggerEnter2D(Collider2D other) => Damage(other);

        void Damage(Collider other)
        {
            if (other.gameObject == null) return;
            if (!damagedLayer.Includes(other.gameObject.layer)) return;
            Damageable damageable = other.GetComponentInParent<Damageable>();
            damageable?.Damage(other, damageAmount);
        }

        void Damage(Collider2D other)
        {
            if (other.gameObject == null) return;
            if (!damagedLayer.Includes(other.gameObject.layer)) return;
            Damageable damageable = other.GetComponentInParent<Damageable>();
            damageable?.Damage(other, damageAmount);
        }
    }
}