using UltEvents;
using UnityEngine;

namespace Game
{
    public class Trigger : MonoBehaviour
    {
        public UltEvent onTriggerEnter2D = default;
        public UltEvent onTriggerExit2D = default;

        private void OnTriggerEnter2D(Collider2D collision)
            => onTriggerEnter2D?.Invoke();

        private void OnTriggerExit2D(Collider2D collision)
             => onTriggerExit2D?.Invoke();
    }
}