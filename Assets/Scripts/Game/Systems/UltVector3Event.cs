using UnityEngine;

namespace Game
{
	[System.Serializable]
	public class UltVector3Event : UltEvents.UltEvent<Vector3> { }
}