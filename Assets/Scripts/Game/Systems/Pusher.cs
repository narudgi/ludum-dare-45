using UnityEngine;

namespace Game
{
    public class Pusher : MonoBehaviour
    {
		public LayerMask triggerMask = default;
		public UltColliderEvent onTrigger = default;

        float force = 1f;
		Pushable self = default;

		private void Awake()
		{
			self = gameObject.GetComponentInParent<Pushable>();
		}

        public void UpdateForce(float force)
        {
            this.force = force;
        }

		private void OnTriggerEnter(Collider other)
		{
			Pushable pushable = other?.GetComponentInParent<Pushable>();
			if (self != null)
				if (self.Equals(pushable)) return;
			if (!triggerMask.Includes(other.gameObject.layer)) return;


			Invulnerablity invulnerablity = other?.GetComponentInParent<Invulnerablity>();
			if (invulnerablity != null)
				if (!invulnerablity.IsInvulnerable)
					onTrigger?.Invoke(other);

			pushable?.Push(transform.position, force);
		}
	}
}