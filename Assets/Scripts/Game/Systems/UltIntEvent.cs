//Copyright (c) Ewan Argouse - http://narudgi.github.io/

using UltEvents;

namespace Game
{
    [System.Serializable]
    public class UltIntEvent : UltEvent<int> { }
}