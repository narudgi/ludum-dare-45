using UnityAtoms;
using UnityEngine;

namespace Game
{
    public class Collecter : MonoBehaviour
    {
        [SerializeField] private LayerMask collectLayer = default;
        [SerializeField] private IntVariable outputValue = default;
        [Header("Clamp")]
        [SerializeField] private IntReference min = default;
        [SerializeField] private IntReference max = default;

        public UltIntEvent onCollect = default;
        private int value = 0;
        public int Value
        {
            get => value;
            private set
            {
                value = Mathf.Clamp(value, min, max);
                this.value = value;
                this.outputValue?.SetValue(this.value);
            }
        }

        private void OnTriggerEnter(Collider other) => Collect(other);
        private void OnTriggerEnter2D(Collider2D other) => Collect(other);

        void Collect(Collider other)
        {
            if (other.gameObject == null) return;
            if (!collectLayer.Includes(other.gameObject.layer)) return;
            Collectable collectable = other.GetComponentInParent<Collectable>();
            Value += collectable?.Collect() ?? 0;
            onCollect?.Invoke(Value);
        }

        void Collect(Collider2D other)
        {
            if (other.gameObject == null) return;
            if (!collectLayer.Includes(other.gameObject.layer)) return;
            Collectable collectable = other.GetComponentInParent<Collectable>();
            Value += collectable?.Collect() ?? 0;
            onCollect?.Invoke(Value);
        }
    }
}