using UltEvents;
using UnityEngine;
using UnityEngine.Events;

namespace Game
{
    public class Collectable : MonoBehaviour
    {
        public int value = 1;
        public UnityEvent onCollect = default;
        public UltEvent onCollectUlt = default;
        public bool destroyOnCollect = false;

        public int Collect()
        {
            onCollect?.Invoke();
            onCollectUlt?.Invoke();
            if (destroyOnCollect)
                Destroy(this.gameObject);
            return value; 
        }
    }
}