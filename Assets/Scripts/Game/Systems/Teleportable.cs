using UnityEngine;

namespace Game
{
    public class Teleportable : MonoBehaviour
    {
        [SerializeField] LayerMask teleportLayer = default;
        [SerializeField] Transform teleportTransform = default;

        private void OnTriggerEnter2D(Collider2D other)
        {
            Trigger(other);
        }

        private void Trigger(Collider2D other)
        {
            if (other.gameObject == null) return;
            if (!teleportLayer.Includes(other.gameObject.layer)) return;
            Teleporter teleporter = other.GetComponentInParent<Teleporter>();
            teleporter?.Teleport(teleportTransform);
        }
    }
}