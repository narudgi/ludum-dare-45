﻿//Copyright (c) Ewan Argouse - http://narudgi.github.io/

using UnityAtoms;
using UnityEngine;

namespace Game
{
	public class Damageable : AtomMonoBehaviour
	{
        public UltColliderFloatEvent OnDamage = default;
        public UltCollider2DFloatEvent OnDamage2D = default;

        public void Damage(Collider other, float dmg) => OnDamage?.Invoke(other, dmg);
		public void Damage(Collider2D other, float dmg) => OnDamage2D?.Invoke(other, dmg);
    }
}