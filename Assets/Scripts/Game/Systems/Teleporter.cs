using UnityAtoms;
using UnityEngine;

namespace Game
{
    public class Teleporter : MonoBehaviour
    {
        public enum TeleporterType { GoTo, ID }

        [SerializeField] TeleporterType teleporterType = default;
        [SerializeField] StringReference idName = default;

        public string ID => idName.Value;
        public bool IsID => teleporterType.Equals(TeleporterType.ID);

        public void Teleport(Transform teleportable)
        {
            Teleporter[] teleporters = FindObjectsOfType<Teleporter>();
            foreach (Teleporter teleporter in teleporters)
            {
                if (ID.Equals(teleporter.ID) && teleporter.IsID)
                {
                    Rigidbody2D rigidbody = teleportable.GetComponent<Rigidbody2D>();
                    rigidbody?.MovePosition(teleporter.transform.position);
                    if (rigidbody != null)
                        rigidbody.velocity = Vector2.zero;
                    teleportable.position = teleporter.transform.position;
                }
            }
        }
    }
}